<?php

declare(strict_types=1);

namespace Wx1860\WxCommon\Utils;

use Psr\Http\Message\ResponseInterface;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Wx1860\WxCommon\Constants\Code;

trait ResponseTrait
{
    /**
     * @param  array  $data
     * @return ResponseInterface
     */
    public function succeed($data = [])
    {
        $result = [
            'code' => 200,
            'message' => '',
            'data' => $data,
        ];
        if(!$data){
            unset($result['data']);
        }

        return $this->json($result);
    }

    /**
     * @param  int  $status
     * @param  int  $code
     * @param  string  $message
     * @param  array  $data
     * @return ResponseInterface
     */
    public function fail(int $status = 500,int $code = -1,$message = '', $data = [])
    {
        $result = [
            'code' => $code,
            'message' => $message,
            'data' => $data,
        ];
        if(!$data){
            unset($result['data']);
        }

        return $this->json($result,$status);
    }

    /**
     * @param  array  $data
     * @param  int  $status
     * @param  array  $headers
     * @param  int  $options
     * @return ResponseInterface
     */
    public function json($data = [], $status = 200, array $headers = [], $options = 0)
    {
        $response = new ResponseInterface();
        $response->withStatus($status);

        foreach ($headers as $name => $value){
            $response->withAddedHeader($name,$value);
        }

        return $response->withBody(new SwooleStream(json_encode($data, JSON_UNESCAPED_UNICODE)));
    }
}