<?php

declare(strict_types=1);

namespace Wx1860\WxCommon;

use Wx1860\WxCommon\Exception\Handler\AppServiceExceptionHandler;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'exceptions' => [
                'handler' => [
                    'http' => [
                        AppServiceExceptionHandler::class,
                    ],
                ]
            ],
        ];
    }
}
