<?php

declare(strict_types=1);

namespace Wx1860\WxCommon\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

class Code extends AbstractConstants
{
    /**
     * @Message("系统繁忙");
     */
    const SYSTEM_ERROR = -1;

    /**
     * @Message("请求成功");
     */
    const SUCCESS = 0;

    /**
     * @Message("无效签名");
     */
    const INVALID_SIGNATURE = 10001;

    /**
     * @Message("key不正确或过期");
     */
    const INVALID_KEY = 10002;

    /**
     * @Message("服务不可用");
     */
    const SERVICE_NOT_AVAILABLE = 10003;

    /**
     * @Message("无效参数");
     */
    const INVALID_PARAMS = 10004;

    /**
     * @Message("非法请求");
     */
    const ILLEGAL_REQUEST = 10005;


}